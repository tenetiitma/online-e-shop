package domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "groupp_id")
    private Group groupp;

    public Category(int id, String name, Group groupp) {
        this.id = id;
        this.name = name;
        this.groupp = groupp;
    }

    public Category() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Group getGroupp() {
        return groupp;
    }

    public void setGroupp(Group groupp) {
        this.groupp = groupp;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
