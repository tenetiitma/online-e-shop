import domain.Category;
import domain.Group;
import domain.Product;
import domain.Specification;
import repository.*;

public class App {

    public static void main(String[] args) {

        GroupRepository groupRepository = new GroupRepository();
        CategoryRepository categoryRepository = new CategoryRepository();
        ProductRepository productRepository = new ProductRepository();
        SpecificationRepository specificationRepository = new SpecificationRepository();
        UserRepository userRepository = new UserRepository();
        OrderRepository orderRepository = new OrderRepository();

    }
}
