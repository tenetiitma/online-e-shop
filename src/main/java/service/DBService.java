package service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class DBService {


    private DBService() {

    }


    public static EntityManagerFactory getEntityManagerFactory () {

        return EmfHandler.getFactory();
    }




    private static class EmfHandler {

        private static EntityManagerFactory emf;

        private static EntityManagerFactory getFactory () {

            if(emf == null) {
                emf = Persistence.createEntityManagerFactory("sample");
            }

            return emf;
        }

    }

}
