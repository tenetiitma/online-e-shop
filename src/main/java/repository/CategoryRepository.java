package repository;

import domain.Category;
import service.DBService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class CategoryRepository {

    private final EntityManagerFactory entityManagerFactory = DBService.getEntityManagerFactory();

    public void saveCategory(Category category) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(category);
            em.getTransaction().commit();
            System.out.println("Category added!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void updateCategory(Category category) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(category);
            em.getTransaction().commit();
            System.out.println("Category updated!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void deleteCategory(int id) {
        String sql = "DELETE Category WHERE id = :categoryId";
        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        int result = em.createQuery(sql).setParameter("categoryId", id).executeUpdate();

        em.getTransaction().commit();
        if (result > 0) {
            System.out.println("Category deleted!");
        }
    }

    public List<Category> getCategories () {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        return em.createQuery("from Category ", Category.class).getResultList();

    }
}
